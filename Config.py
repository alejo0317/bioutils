import MySQLdb

#Cristian Alejandro Rojas
#carojasq@correo.udistrital.edu.co

#El presente archivo define los parametros de conexion a la base de datos y devuelve un objeto cursor para hacer las consultas SQL con el

def connect():
	#Se definen los parametros de conexion a la base de datos
	host="localhost"
	usuario="root"
	password="radom_passwordatabase"
	db_name="resultados"
	try:
		db=MySQLdb.connect(host=host,user=usuario,passwd=password,db=db_name)
		try: 
			db.autocommit(True)
		except:
			print "No fue posible poner autocommit en su conexion, por favor revise los parametros de configuracion de su base de datos"
			quit()
	except:
		print "Se produjo un error en la conexion con la base de datos"
		quit()
	cursor = db.cursor(MySQLdb.cursors.DictCursor) 
	return cursor
global dir;
dir="/var/www/html/a2"
