#This module stores several blast formats in a table for easy-query

import sys
import os
from Bio.Blast import NCBIXML
from BioUtils.Misc import QueryGenerator as QG
from BioUtils import Config


#Stores XML result of BLAST in a table
def BLASTXMLtoDB(xml_name, table):

	def hit2accessions(hit_def):
		accessions={}
		accessions['ref']=""
		accessions['gi']=""
		accessions['uniprot']=""
		hit_defs=hit_def.split("|")
		if "ref" in hit_defs:
			indice=hit_defs.index("ref")
			accessions['ref']=hit_defs[indice+1].split(".")[0]
		if "gi" in hit_defs:
			indice=hit_defs.index("gi")
			accessions['gi']=hit_defs[indice+1].split(".")[0]
		if "sp" in hit_defs:
			indice=hit_defs.index("sp")
			accessions['uniprot']=hit_defs[indice+1].split(".")[0]
		return accessions


	def addtodb(filename, tabla):
		entrada_xml=open(filename)
		records=NCBIXML.parse(entrada_xml)
		contadorcompleto=0
		for record in records:
			for alignment in record.alignments[0:1]:	
				#Solo toma la parte del query id que da el id del query real.
				query_id=record.query.split(" ")[0]
				hit_def=alignment.hit_def
				alignment_length=alignment.hsps[0].align_length
				e_value=str(alignment.hsps[0].expect)
				bit_score=str(alignment.hsps[0].bits)
				identities= alignment.hsps[0].identities
				score=alignment.hsps[0].score
				accessions=hit2accessions(hit_def)
				gi_id=accessions['gi']
				ref_id=accessions['ref']
				uniprot_id=accessions['uniprot']
				coverage=(float(alignment.hsps[0].query_end - alignment.hsps[0].query_start)/record.query_letters)
				row={}
				row['query_id']=query_id
				row['align_length']=alignment_length
				row['geneinfo_id']=accessions['gi']
				row['refseq_id']=accessions['ref']
				row['uniprot_id']=accessions['uniprot']
				row['e_value']=e_value
				row['hit_def']=hit_def
				row['bitscore']=bit_score
				row['score']=score
				row['identities']=identities
				row['coverage']=coverage
				cursor=Config.connect()
				query=QG.gen_insert(tabla, row)
				try:
					cursor.execute(query)
				except:
					#print "Error en el query "+query
					continue
				contadorcompleto=contadorcompleto+1
		return contadorcompleto

	def createtable(nombre_tabla):
		cursor=Config.connect()
		query="CREATE TABLE IF NOT EXISTS `%s` (`query_id` varchar(100) NOT NULL,  `align_length` int(11) NOT NULL,  `geneinfo_id` varchar(10) NOT NULL,   `refseq_id` varchar(15) NOT NULL,   `uniprot_id` varchar(10) NOT NULL,   `e_value` double NOT NULL,   `hit_def` varchar(400) NOT NULL,   `coverage` double NOT NULL, `bitscore` double NOT NULL,   `score` int(11) NOT NULL,   `identities` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;" % nombre_tabla
		cursor.execute(query)	
		return nombre_tabla

	createtable(table)
	contadorcompleto=addtodb(xml_name, table)
	return contadorcompleto
	
	

