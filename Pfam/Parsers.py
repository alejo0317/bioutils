#Parsers for severall Pfam_scan outputs


def Pfam_dat_parser(file_in):
#    import ipdb; ipdb.set_trace()
    file_object = open(file_in, "r")
    parsed = {}
    for line in file_object:
        if line[0] == "#":
            continue
        fields = filter(None , line.replace("\n", "").split(" "))
        if not parsed.has_key(fields[3]):
            parsed[fields[3]] = {
                'subject' : [fields[1]]
            }
        else:
            parsed[fields[3]]['subject'].append(fields[1])
    return parsed


